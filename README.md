## About the repo
Generates 100 prime numbers using the same library/module code in fluter_first_app
## Commands

```
make clean
make build-android
make build-flutter 
```

## Assumptions

On Native
1. No UI screens in kotlin/swift
2. Only MainActivity and that will be inherited by FlutterActivity and immediately launches flutter
3. Any native code will be wrapped in plugins
4. No multidexing/progaurd considerations


## Benchmarking

### Add-to-app with android booting up flutteractivity directly 

| Metric                     | Add-to-App                                                                                                                        | Flutter-first                                                                     |
| -------------------------- | --------------------------------------------------------------------------------------------------------------------------------- | --------------------------------------------------------------------------------- |
| Hot reload                 | Not possible from shipping android app but only from my_fluttter_module [Video Link](resources/Add%20to%20app%20hot%20reload.mp4) | Normal hot reload [Video Link](resources/flutter%20app%20hot%20reload%20demo.mp4) |
| Debugging with breakpoints | Directly                                                                                                                          | Possible with flutter [attach](https://docs.flutter.dev/add-to-app/debugging)     |
|                            |

|                add-to-app                | M1 air | M1 pro 64gb ram |
| ------------------------------------ | ---------- | ------------- |
| apk size  | 147MB | 72MB
| build time | 20 sec | 20sec

|                flutter-app                | M1 air | M1 pro 64gb ram |
| ------------------------------------ | ---------- | ------------- |
| apk size  | 49MB | 19MB
| build time | 35sec | 30 sec

### Add-to-app with method channel communications

| Metric                               | Add-to-App | Flutter-first |
| ------------------------------------ | ---------- | ------------- |
| Management of method channels        |            |               |
| Flutter engine management            | Explicit   | Implicit      |
| Memory Used for Intensive Operations |            |               |

