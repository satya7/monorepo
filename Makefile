clean: 
	cd my_flutter_module && flutter clean
	cd my_flutter_package && flutter clean
	cd flutter_first_app && flutter clean
	@test -f my_flutter_module/.android/include_flutter.groovy && (cd android_fullscreen && ./gradlew clean) || exit 0

build-android: clean
	cd my_flutter_module && flutter pub get
	cd android_fullscreen && ./gradlew assembleDebug

build-flutter: clean
	cd flutter_first_app && flutter build apk --no-tree-shake-icons
