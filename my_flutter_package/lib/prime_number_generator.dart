// prime_number_generator.dart in my_flutter_package

class PrimeNumberGenerator {
  List<int> _primeNumbers = [];

  List<int> get primeNumbers => List.unmodifiable(_primeNumbers);

  void generateNext100Primes() {
    int count = 0;
    int currentNumber = _primeNumbers.isEmpty ? 2 : _primeNumbers.last + 1;

    while (count < 100) {
      if (_isPrime(currentNumber)) {
        _primeNumbers.add(currentNumber);
        count++;
      }
      currentNumber++;
    }
  }

  bool _isPrime(int number) {
    for (var i = 2; i <= number / 2; i++) {
      if (number % i == 0) {
        return false;
      }
    }
    return true;
  }
}
