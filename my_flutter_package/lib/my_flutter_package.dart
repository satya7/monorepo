library my_flutter_package;

// import 'package:flutter/material.dart';
// import 'dart:async';

export 'prime_number_generator.dart';

// void main() {
//   runApp(MyApp());
// }

// class MyApp extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       title: 'Prime Number Generator',
//       theme: ThemeData(
//         primarySwatch: Colors.blue,
//       ),
//       home: PrimeNumberGeneratorPage(),
//     );
//   }
// }

// class PrimeNumberGeneratorPage extends StatefulWidget {
//   @override
//   _PrimeNumberGeneratorPageState createState() =>
//       _PrimeNumberGeneratorPageState();
// }

// class _PrimeNumberGeneratorPageState extends State<PrimeNumberGeneratorPage> {
//   bool _isGenerating = false;
//   List<int> _primeNumbers = [];
//   Timer? _timer;
//   int _startNumber = 1000000; // Starting from one million

//   void _toggleGenerating() {
//     if (_isGenerating) {
//       _timer?.cancel();
//     } else {
//       // Start from the last found prime number or _startNumber
//       int currentNumber =
//           _primeNumbers.isNotEmpty ? _primeNumbers.last + 1 : _startNumber;

//       _timer = Timer.periodic(Duration(milliseconds: 100), (Timer t) {
//         while (!_isPrime(currentNumber)) {
//           currentNumber++;
//         }
//         _primeNumbers.add(currentNumber++);
//         setState(() {});
//       });
//     }

//     setState(() {
//       _isGenerating = !_isGenerating;
//     });
//   }

//   bool _isPrime(int number) {
//     for (var i = 2; i <= number / 2; i++) {
//       if (number % i == 0) {
//         return false;
//       }
//     }
//     return true;
//   }

//   @override
//   void dispose() {
//     _timer?.cancel();
//     super.dispose();
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Text('Prime Number Generator'),
//       ),
//       body: Center(
//         child: Column(
//           mainAxisAlignment: MainAxisAlignment.center,
//           children: <Widget>[
//             Text(
//               'Generated Prime Numbers:',
//             ),
//             Expanded(
//               child: ListView.builder(
//                 itemCount: _primeNumbers.length,
//                 itemBuilder: (context, index) => ListTile(
//                   title: Text('${_primeNumbers[index]}'),
//                 ),
//               ),
//             ),
//             ElevatedButton(
//               onPressed: _toggleGenerating,
//               child:
//                   Text(_isGenerating ? 'Stop Generating' : 'Start Generating'),
//             ),
//           ],
//         ),
//       ),
//     );
//   }
// }
