import 'package:flutter/material.dart';
import 'package:my_flutter_package/prime_number_generator.dart'; // Import your package

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Prime Number Generator App',
      home: PrimeNumberScreen(),
    );
  }
}

class PrimeNumberScreen extends StatefulWidget {
  @override
  _PrimeNumberScreenState createState() => _PrimeNumberScreenState();
}

class _PrimeNumberScreenState extends State<PrimeNumberScreen> {
  final PrimeNumberGenerator _generator = PrimeNumberGenerator();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Prime Numbers flutter app'),
      ),
      body: ListView.builder(
        itemCount: _generator.primeNumbers.length,
        itemBuilder: (context, index) {
          return ListTile(
            title: Text(_generator.primeNumbers[index].toString()),
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          setState(() {
            _generator.generateNext100Primes();
          });
        },
        child: Icon(Icons.add),
        tooltip: 'Generate Next 100 Primes',
      ),
    );
  }
}
